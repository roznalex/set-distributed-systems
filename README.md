# Replicated Log Project

## Overview

The Replicated Log Project is designed to offer a robust solution for message logging and replication across a distributed system. It features a Master-Secondary deployment architecture where messages are consistently replicated from the Master server to any number of Secondary servers, ensuring high availability and fault tolerance.

## Architecture

### Master Server

- **HTTP Server**:
  - **POST `/msgs`**: Appends a message to an in-memory list and replicates it to all Secondary servers.
  - **GET `/msgs`**: Retrieves all messages from the in-memory list.
  - **GET `/health`**: Checks and returns the health status of all Secondary servers.
- **Replication**: Implements tunable semi-synchronicity for replication with write concern parameters (w=1,2,3,..,n).
- **Failover Strategy**: Switches to read-only mode if a quorum is not achieved, enhancing data integrity.

### Secondary Servers

- **HTTP Server**:
  - **GET `/msgs`**: Returns all replicated messages from the in-memory list.
- **Data Consistency**: Ensures total order of messages and implements deduplication to maintain a consistent state.
- **Health Monitoring**: Responds to heartbeat signals from the Master to indicate its status.

## Prerequisites

- **Docker**: Ensure Docker and Docker Compose are installed on your machine.
- **Environment**: The project is containerized for easy deployment and isolation.

## Using the Project

Follow these steps to get your Replicated Log system up and running:

1. **Clone the repository**:
   ```bash
   git clone <repository-url>
   ```
2. **Navigate to the project directory**:
   ```bash
   cd replicated-log-project
   ```
3. **Start the system**:
   ```bash
   docker-compose up
   ```

### Interacting with the System

- **Append a message** (with write concern `w`):
  ```bash
  curl -X POST -d "msg=YourMessage&w=2" http://localhost:8000/msgs
  ```
- **Retrieve all messages**:
  ```bash
  curl http://localhost:8000/msgs
  ```
- **Check Secondary servers' health**:
  ```bash
  curl http://localhost:8000/health
  ```

### Simulating Scenarios

- **Stop a Secondary server** (to test failover and replication recovery):
  ```bash
  docker stop <container_name_of_secondary>
  ```
- **Restart the Secondary server** (to observe state synchronization):
  ```bash
  docker start <container_name_of_secondary>
  ```

In addition to starting and stopping Secondary servers, you can perform a series of tests to ensure the system behaves as expected under various conditions. Below are `curl` scripts designed for a Self-check acceptance test:

#### Self-check Acceptance Test

1. **Ensure Master (M) and Secondary 1 (S1) are running**.
   - This is typically done via `docker-compose up`, but verify that both the Master and Secondary 1 services are correctly configured and operational.

2. **Send (Msg1, W=1) - Ok**:
   ```bash
   curl -X POST http://localhost:8000/msgs -d "msg=Msg1&w=1"
   ```

3. **Send (Msg2, W=2) - Ok**:
   ```bash
   curl -X POST http://localhost:8000/msgs -d "msg=Msg2&w=2"
   ```

4. **Send (Msg3, W=3) - Wait**:
   - Adjust the write concern (`w`) based on your number of nodes.
   ```bash
   curl -X POST http://localhost:8000/msgs -d "msg=Msg3&w=3"
   ```

5. **Send (Msg4, W=1) - Ok**:
   ```bash
   curl -X POST http://localhost:8000/msgs -d "msg=Msg4&w=1"
   ```

6. **Start Secondary 2 (S2)**.
   - This involves bringing another Secondary server online, depending on your setup method.

7. **Check messages on S2 - [Msg1, Msg2, Msg3, Msg4]**:
   - Verify Secondary 2 has synchronized correctly upon joining.
   ```bash
   curl http://localhost:8002/msgs
   ```