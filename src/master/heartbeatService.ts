// HeartbeatService.ts
import logger from "../common/utils/logger.ts";
import { sendHeartbeat } from "../common/utils/request.ts";

export class HeartbeatService {
  private secondaryServers: string[];
  private healthStatus: Record<string, string>;

  constructor(secondaryServers: string[] = []) {
    this.secondaryServers = secondaryServers;
    this.healthStatus = secondaryServers.reduce(
      (acc: Record<string, string>, server) => {
        acc[server] = "Healthy";
        return acc;
      },
      {},
    );
  }

  public startHeartbeat() {
    this.secondaryServers.forEach((server) => {
      setInterval(async () => {
        try {
          await sendHeartbeat(server);
          this.healthStatus[server] = "Healthy";
        } catch (error) {
          logger.error(`Error sending heartbeat to ${server}: ${error}`);
          this.healthStatus[server] = this.healthStatus[server] === "Healthy"
            ? "Suspected"
            : "Unhealthy";
        }
      }, Number(Deno.env.get("HEARTBEAT_INTERVAL")) || 60000);
    });
  }

  public getHealthStatus() {
    return this.healthStatus;
  }

  public getServerHealthStatus(server: string): string {
    logger.info(`${server}'s health status: ${this.healthStatus[server]}`);
    return this.healthStatus[server] || "Unknown";
  }

  public countHealthyServers(): number {
    return Object
      .values(this.healthStatus)
      .filter((status) => status === "Healthy").length;
  }
}
