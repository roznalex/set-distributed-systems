import { Application, httpLogger } from "../common/deps.ts";
import logger from "../common/utils/logger.ts";
import envConfig from "../common/utils/env.ts";
import msgRoute from "./msgRouter.ts";
import { heartbeatService } from "./services.ts";

const app = new Application();
heartbeatService.startHeartbeat();

app.use(httpLogger());
app.start({ port: envConfig.port });

app.get("/health", () => {
  return heartbeatService.getHealthStatus();
});
msgRoute(app.group("msgs"));

logger.info(`${envConfig.serverRole} server is running: ${envConfig.port}/`);
