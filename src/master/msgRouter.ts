import {
  BadRequestException,
  Context,
  Group,
  InternalServerErrorException,
} from "../common/deps.ts";
import { MessageParameters } from "../common/types.ts";
import { validateAddMsg } from "./validatorMiddleware.ts";
import { msgService } from "./services.ts";

export default function (g: Group) {
  g.get("/", async (c: Context) => {
    try {
      const { startId, endId } = c.params;
      const startIdNum = startId ? Number(startId) : undefined;
      const endIdNum = endId ? Number(endId) : undefined;

      return await msgService.getMessages(startIdNum, endIdNum);
    } catch (_) {
      throw new InternalServerErrorException("Cannot view the messages.");
    }
  })
    .post("/", async (c: Context) => {
      try {
        const body = await c.body as MessageParameters;
        const writeConcern = body.w ? Number.parseInt(body.w) : 1;

        const message = await msgService.addMessage(body.msg, writeConcern);

        if (!message) {
          throw new InternalServerErrorException("Message was not added");
        }

        return message;
      } catch (error) {
        if (error instanceof BadRequestException) {
          throw error;
        }

        throw new InternalServerErrorException("Cannot add the message");
      }
    }, validateAddMsg);
}
