import { BadRequestException, MiddlewareFunc } from "../common/deps.ts";
import { MessageParameters } from "../common/types.ts";

export const validateAddMsg: MiddlewareFunc = (next) => async (c) => {
  const body = await c.body as MessageParameters;

  if (!body || !body.msg) {
    throw new BadRequestException("Invalid message");
  }

  if (body.w && ![1, 2, 3].includes(Number.parseInt(body.w))) {
    throw new BadRequestException(
      "Invalid write concern parameter. Should be 1, 2 or 3",
    );
  }

  return next(c);
};
