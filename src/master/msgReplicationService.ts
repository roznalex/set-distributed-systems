import { MessageObject } from "../common/types.ts";
import logger from "../common/utils/logger.ts";
import { sendMessage } from "../common/utils/request.ts";
import { HeartbeatService } from "./heartbeatService.ts";

export class MessageReplicationService {
  private _secondaryServers: string[];
  private _heartbeatService: HeartbeatService;

  constructor(
    secondaryServers: string[] = [],
    heartbeatService: HeartbeatService,
  ) {
    this._secondaryServers = secondaryServers;
    this._heartbeatService = heartbeatService;
  }

  public get secondaryServers() {
    return this._secondaryServers;
  }

  public hasQuorum(): boolean {
    const requiredQuorum = Math.ceil((this._secondaryServers.length + 1) / 2);
    return this._heartbeatService.countHealthyServers() + 1 >= requiredQuorum;
  }

  public replicateMsgWithConcern = async (
    messageObject: MessageObject,
    writeConcern: number,
    maxRetries = Number(Deno.env.get("MAX_RETRIES")) || 3,
    retryDelay = Number(Deno.env.get("RETRY_DELAY")) || 10000, // 10 seconds
    timeout = Number(Deno.env.get("TIMEOUT")) || 30000, // 30 seconds
  ): Promise<boolean> => {
    if (!this.hasQuorum()) {
      logger.error("Quorum not met. Cannot proceed with replication.");
      return false;
    }
    let ackCount = 0;

    const replicationTasks = this._secondaryServers.map((server) =>
      this.attemptReplication(
        server,
        messageObject,
        maxRetries,
        retryDelay,
        timeout,
      )
        .then((success) => {
          if (success) ackCount++;
          return ackCount >= writeConcern - 1;
        })
    );

    // Wait for the write concern to be met or all replication tasks to be completed
    for (const task of replicationTasks) {
      const writeConcernMet = await task;
      if (writeConcernMet) {
        // Early exit if write concern is met
        return true;
      }
    }

    return ackCount >= writeConcern - 1;
  };

  private async attemptReplication(
    server: string,
    messageObject: MessageObject,
    maxRetries: number,
    retryDelay: number,
    timeout: number,
  ): Promise<boolean> {
    for (let attempt = 0; attempt < maxRetries; attempt++) {
      const timeoutPromise = new Promise((_, reject) => {
        setTimeout(
          () => reject(new Error(`Timeout reached for ${server}`)),
          timeout,
        );
      });

      const healthStatus = this._heartbeatService.getServerHealthStatus(server);

      if (healthStatus === "Unhealthy") {
        logger.info(`Skipping retry for unhealthy server: ${server}`);
        break;
      }

      try {
        await Promise.race([
          sendMessage(server, messageObject),
          timeoutPromise,
        ]);
        logger.info(`Msg ${messageObject.id} replicated to ${server}`);
        return true;
      } catch (err) {
        logger.error(`Error sending to ${server}: ${err}`);
        if (attempt < maxRetries - 1) {
          await new Promise((resolve) => setTimeout(resolve, retryDelay));
        }
      }
    }
    // Replication failed after retries
    return false;
  }
}
