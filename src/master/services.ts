import { HeartbeatService } from "./heartbeatService.ts";
import envConfig from "../common/utils/env.ts";
import { MessageService } from "./msgService.ts";
import { MessageReplicationService } from "./msgReplicationService.ts";

const heartbeatService = new HeartbeatService(envConfig.secondaryUrls);
const msgReplicationService = new MessageReplicationService(
  envConfig.secondaryUrls,
  heartbeatService,
);
const msgService = new MessageService([], msgReplicationService);

export { heartbeatService, msgReplicationService, msgService };
