import { MessageObject } from "../common/types.ts";
import logger from "../common/utils/logger.ts";
import { MessageReplicationService } from "./msgReplicationService.ts";

export class MessageService {
  private _msgCounter: number;
  private _messages: Record<number, string>;
  private _msgReplicationService: MessageReplicationService;

  constructor(
    messages: string[] = [],
    msgReplicationService: MessageReplicationService,
  ) {
    this._msgCounter = messages.length;
    this._messages = MessageService.initMessages(messages);
    this._msgReplicationService = msgReplicationService;
  }

  static initMessages(messages: string[]): Record<number, string> {
    const messageMap: Record<number, string> = {};
    messages.forEach((msg, index) => {
      messageMap[index + 1] = msg;
    });
    return messageMap;
  }

  public getMessages(startId?: number, endId?: number): MessageObject[] {
    logger.info("Fetching messages", { startId, endId });

    return Object.keys(this._messages)
      .map((id) => Number(id))
      .filter((id) =>
        (startId === undefined || id >= startId) &&
        (endId === undefined || id <= endId)
      )
      .map((id) => ({
        id: id,
        msg: this._messages[id],
      }));
  }

  public async addMessage(
    msg: string,
    writeConcern: number,
  ): Promise<MessageObject | null> {
    if (!this._msgReplicationService.hasQuorum()) {
      logger.info("Quorum not met. Master is in read-only mode.");
      return null;
    }
    const newId = this._msgCounter + 1;
    const messageObject: MessageObject = { id: newId, msg };

    this._messages[newId] = msg;
    this._msgCounter++;
    logger.info(`Added new message: ${newId}:${msg}`);

    try {
      const isReplicated = await this._msgReplicationService
        .replicateMsgWithConcern(
          messageObject,
          writeConcern,
        );
      if (writeConcern > 1 && !isReplicated) {
        logger.error(
          "Failed to meet write concern for message: ",
          messageObject,
        );
        return null;
      }
    } catch (error) {
      logger.error("Failed to replicate message: ", error);
      return null;
    }
    return messageObject;
  }
}
