import { BadRequestException, MiddlewareFunc } from "../common/deps.ts";
import { MessageObject } from "../common/types.ts";

export const validateAddMsg: MiddlewareFunc = (next) => async (c) => {
  const body = await c.body as MessageObject;

  if (!body || !body.msg || !body.id) {
    throw new BadRequestException("Invalid message");
  }

  if (body.id !== undefined && Number.isInteger(body.id) && body.id <= 0) {
    throw new BadRequestException("Invalid id");
  }

  return next(c);
};
