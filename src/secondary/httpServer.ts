import { Application, httpLogger } from "../common/deps.ts";
import logger from "../common/utils/logger.ts";
import envConfig from "../common/utils/env.ts";
import msgRoute from "./msgRouter.ts";

const app = new Application();

app.use(httpLogger());
app.start({ port: envConfig.port });

app.get("/heartbeat", () => {
  return "OK";
});
msgRoute(app.group("msgs"));

logger.info(`${envConfig.serverRole} server is running: ${envConfig.port}/`);
