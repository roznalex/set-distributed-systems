import { MessageObject } from "../common/types.ts";
import logger from "../common/utils/logger.ts";
import { getMessages } from "../common/utils/request.ts";

export class MessageService {
  private _messages: Record<number, string>;
  private _messageQueue: MessageObject[];
  private _masterServer: string;
  private _lastProcessedId = 0;

  constructor(masterServer: string = "") {
    this._messages = {};
    this._messageQueue = [];
    this._masterServer = masterServer;
  }

  static initMessages(messages: string[]): Record<number, string> {
    return messages.reduce((acc, msg, index) => {
      acc[index + 1] = msg;
      return acc;
    }, {} as Record<number, string>);
  }

  public getMessages(): MessageObject[] {
    logger.info("Fetching messages");
    return Object.keys(this._messages)
      .map((id) => Number(id))
      .map((id) => ({ id: id, msg: this._messages[id] }));
  }

  public async addMessage(
    messageObject: MessageObject,
  ): Promise<MessageObject | null> {
    // Check for potential duplication
    if (
      Object.prototype.hasOwnProperty.call(this._messages, messageObject.id)
    ) {
      logger.info(`Duplicate message ID detected: ${messageObject.id}`);
      // Return message without adding a second time
      return messageObject;
    }

    if (messageObject.id === this._lastProcessedId + 1) {
      this.storeMessage(messageObject);
      this.processQueue();
      return messageObject;
    } else {
      this._messageQueue.push(messageObject);
      this._messageQueue.sort((a, b) => a.id - b.id);
      // Message is out-of-order and queued
      return null;
    }
  }

  private async handleOutOfOrderMessage(incomingId: number): Promise<void> {
    const startId = this._lastProcessedId + 1;
    const endId = incomingId - 1;

    if (startId <= endId) {
      const missingMessages = await this.requestMissingMessagesFromMaster(
        startId,
        endId,
      );
      this.processMissingMessages(missingMessages);
    } else {
      logger.error("The latest messages added are incorrect. Delete them ...");
      // Delete messages from the secondary server that are beyond the last confirmed ID
      this.deleteExtraMessages(incomingId);
    }
  }

  private deleteExtraMessages(upToId: number): void {
    Object.keys(this._messages)
      .map(Number)
      .filter((id) => id >= upToId)
      .forEach((id) => {
        delete this._messages[id];
      });
    this._lastProcessedId = upToId - 1;
  }

  private storeMessage(messageObject: MessageObject): void {
    this._messages[messageObject.id] = messageObject.msg;
    this._lastProcessedId = messageObject.id;
  }

  private processQueue(): void {
    while (
      this._messageQueue.length > 0 &&
      this._messageQueue[0].id === this._lastProcessedId + 1
    ) {
      const messageObject = this._messageQueue.shift();
      this.storeMessage(messageObject!);
    }
  }

  private requestMissingMessagesFromMaster = async (
    startId?: number,
    endId?: number,
  ): Promise<MessageObject[]> => {
    const masterMessages = await getMessages(
      this._masterServer,
      startId,
      endId,
    );
    logger.info(
      `Received messages from master: ${JSON.stringify(masterMessages)}`,
    );
    return masterMessages;
  };

  private processMissingMessages(missingMessages: MessageObject[]): boolean {
    if (missingMessages.length === 0) {
      logger.error("No missing messages received for catch-up.");
      return false;
    }

    // Verify that the missing messages correctly fill the gap
    const expectedNextId = this._lastProcessedId + 1;
    if (missingMessages[0].id !== expectedNextId) {
      logger.error("The first missing message does not fill the expected gap.");
      return false;
    }

    missingMessages.forEach((message) => {
      this._messages[message.id] = message.msg;
      this._lastProcessedId = message.id;
    });

    return true;
  }
}
