import {
  BadRequestException,
  Context,
  Group,
  InternalServerErrorException,
} from "../common/deps.ts";
import { MessageService } from "./msgService.ts";
import { MessageObject } from "../common/types.ts";
import envConfig from "../common/utils/env.ts";
import { validateAddMsg } from "./validatorMiddleware.ts";

export default function (g: Group) {
  const msgService = new MessageService(envConfig.masterUrl);

  g.get("/", async () => {
    try {
      return await msgService.getMessages();
    } catch (_) {
      throw new InternalServerErrorException("Cannot view the messages.");
    }
  })
    .post("/", async (c: Context) => {
      try {
        const body = await c.body as MessageObject;
        const message = await msgService.addMessage(body);

        if (!message) {
          throw new InternalServerErrorException("Message was not added");
        }

        return body;
      } catch (error) {
        if (error instanceof BadRequestException) {
          throw error;
        }

        throw new InternalServerErrorException("Cannot add the message");
      }
    }, validateAddMsg);
}
