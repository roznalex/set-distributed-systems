export interface MessageParameters {
  msg: string;
  w: string;
}

export type MessageObject = {
  id: number;
  msg: string;
};

export interface EnvConfig {
  port: number;
  secondaryUrls: string[] | undefined;
  masterUrl: string | undefined;
  serverRole: string;
}