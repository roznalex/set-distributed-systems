import { EnvConfig } from "../types.ts";

const tempConfig = {
  port: Deno.env.get("PORT"),
  secondaryUrls: Deno.env.get("SECONDARY_URLS"),
  masterUrl: Deno.env.get("MASTER_URL"),
  serverRole: Deno.env.get("ROLE") || "master",
};

if (tempConfig.serverRole === "master") {
  if (
    tempConfig.secondaryUrls === undefined ||
    tempConfig.secondaryUrls.length === 0
  ) {
    throw Error(
      `Env variables are not initialized: secondaryUrls:${tempConfig.secondaryUrls}`,
    );
  }
}

if (tempConfig.serverRole === "secondary") {
  if (!tempConfig.masterUrl) {
    throw Error(
      `Env variables are not initialized: masterUrl:${tempConfig.secondaryUrls}`,
    );
  }
}

const envConfig: EnvConfig = {
  port: parseInt(tempConfig.port!),
  secondaryUrls: tempConfig.secondaryUrls?.split(","),
  masterUrl: tempConfig.masterUrl,
  serverRole: tempConfig.serverRole!,
};

export default envConfig;
