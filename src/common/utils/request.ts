import { MessageObject } from "../types.ts";
import logger from "./logger.ts";

export function getMessages(server: string, startId?: number, endId?: number) {
  const queryParams = new URLSearchParams();

  if (startId !== undefined) {
    queryParams.append("startId", startId.toString());
  }
  if (endId !== undefined) {
    queryParams.append("endId", endId.toString());
  }

  const endpointUrl = `${server}/msgs?${queryParams}`;

  logger.info(`Sending request to: ${endpointUrl}`);

  const requestOptions = {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  };

  return fetch(endpointUrl, requestOptions)
    .then(handleResponse)
    .catch(handleError);
}

export function sendMessage(server: string, messageObject: MessageObject) {
  const endpointUrl = `${server}/msgs`;
  const { id, msg } = messageObject;

  logger.info(`Sending request to: ${endpointUrl}`);

  const payload = JSON.stringify({ id, msg });

  const requestOptions = {
    method: "POST",
    body: payload,
    headers: {
      "content-type": "application/json",
    },
  };

  return fetch(endpointUrl, requestOptions)
    .then(handleResponse)
    .catch(handleError);
}

export function sendHeartbeat(server: string) {
  const endpointUrl = `${server}/heartbeat`;

  logger.info(`Sending heartbeat to: ${endpointUrl}`);

  const requestOptions = {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  };

  return fetch(endpointUrl, requestOptions)
    .then(handleResponse)
    .catch(handleError);
}

function handleResponse(response: Response) {
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }

  const contentType = response.headers.get("content-type");

  if (contentType && contentType.includes("application/json")) {
    return response.json();
  }

  return response.text();
}

function handleError(error: Error) {
  logger.error(`Error: ${error.message}`);
  return Promise.reject(error);
}
